import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';
import 'firebase/compat/storage';

const app = firebase.initializeApp({
  apiKey: "AIzaSyCits9zzNK2ey8JKu0wPyk5-mAW4hLYtho",
  authDomain: "community-health-care-uop.firebaseapp.com",
  databaseURL: "https://community-health-care-uop-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "community-health-care-uop",
  storageBucket: "community-health-care-uop.appspot.com",
  messagingSenderId: "308174125314",
  appId: "1:308174125314:web:84cf2d48eb95766dbeea1b",
  measurementId: "G-PMHBWMZR2X"
})

export const auth = app.auth();
export const firestore = app.firestore();
export const storage = app.storage();
export default app