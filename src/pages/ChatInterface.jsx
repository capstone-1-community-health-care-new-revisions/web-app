import React, { useState, useEffect, useRef } from 'react';
import { addDoc, collection, serverTimestamp, onSnapshot, query, orderBy, doc, setDoc } from 'firebase/firestore';
import { firestore } from '../firebase.js';
import { format } from 'date-fns';
import { FaArrowLeft, FaPaperPlane } from 'react-icons/fa';
import '../assets/css/ChatInterface.css';

const ChatInterface = ({ conversation, currentUser, onBackButtonClick, otherUser }) => {
  const [message, setMessage] = useState('');
  const [messages, setMessages] = useState([]);
  const messageListRef = useRef(null);

  const sendMessage = async () => {
    try {
      if (!message.trim()) {
        return;
      }

      const newMessage = {
        text: message,
        senderId: currentUser.id,
        displayName: currentUser.displayName,
        photoURL: currentUser.photoURL,
        timestamp: serverTimestamp()
      };

      // Add message to the main conversation collection
      await addDoc(collection(firestore, 'conversations', conversation.conversationId, 'messages'), newMessage);

      // Update the lastMessage field within the conversation document
      const conversationRef = doc(firestore, 'conversations', conversation.conversationId);
      await setDoc(conversationRef, { lastMessage: newMessage }, { merge: true });

      setMessage('');
    } catch (error) {
      console.error('Error sending message:', error);
    }
  };

  useEffect(() => {
    const unsubscribe = onSnapshot(
      query(collection(firestore, 'conversations', conversation.conversationId, 'messages'), orderBy('timestamp')),
      (snapshot) => {
        const newMessages = snapshot.docs.map((doc) => ({
          id: doc.id,
          ...doc.data()
        }));
        setMessages(newMessages);

        scrollToBottom();
      },
      (error) => {
        console.error('Error fetching messages:', error);
      }
    );

    return () => unsubscribe();
  }, [conversation]);

  useEffect(() => {
    scrollToBottom();
  }, [messages]);

  const scrollToBottom = () => {
    if (messageListRef.current) {
      messageListRef.current.scrollTop = messageListRef.current.scrollHeight;
    }
  };

  const formatDate = (timestamp) => {
    return timestamp ? format(timestamp.toDate(), 'h:mm aa') : 'Invalid Timestamp';
  };

  const handleInputChange = (event) => {
    setMessage(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    if (message.trim() !== '' && currentUser) { 
      sendMessage();
    }
  };

  return (
    <div className="chat-interface-border">
      <button onClick={onBackButtonClick} className="back-button">
        <FaArrowLeft />
      </button>
      <div className="chat-interface-container">
        <div ref={messageListRef} className="message-list">
          {messages.map((message, index) => (
            <div key={index} className={message.senderId === currentUser.id ? 'user-message' : 'other-message'}>
              <div className="message-content">
                {message.senderId !== currentUser.id ? (
                  <div className="sender-info">
                    <img src={otherUser.photoURL} alt={otherUser.displayName} className="sender-photo" />
                    <span className="sender-name">{otherUser.displayName}</span>
                  </div>
                ) : (
                  <div className="sender-info">
                    <img src={currentUser.photoURL} alt={currentUser.displayName} className="sender-photo" />
                    <span className="sender-name">{currentUser.displayName}</span>
                  </div>
                )}
                <div className="text">
                  {message.text}
                </div>
                <div className="timestamp">{formatDate(message.timestamp)}</div>
              </div>
            </div>
          ))}
        </div>
        <form onSubmit={handleSubmit} className="message-form">
          <input type="text" value={message} onChange={handleInputChange} placeholder="Type your message..." className="message-input" />
          <button type="submit" className="send-button">
            <FaPaperPlane />
          </button>
        </form>
      </div>
    </div>
  );
};

export default ChatInterface;
