import React, { useState, useEffect } from 'react';
import { collection, onSnapshot, doc, getDoc } from 'firebase/firestore';
import { firestore } from '../firebase.js';
import { auth } from '../firebase.js';
import ChatInterface from './ChatInterface';
import '../assets/css/Chat.css';
import { format } from 'date-fns'; // Import format function for date formatting
import { FaBell } from 'react-icons/fa';


const Chat = () => {
  const [selectedConversation, setSelectedConversation] = useState(null);
  const [users, setUsers] = useState([]);
  const [currentUser, setCurrentUser] = useState(null);
  const [searchTerm, setSearchTerm] = useState('');
  const [lastMessages, setLastMessages] = useState({});

  useEffect(() => {
    const unsubscribeUsers = onSnapshot(collection(firestore, 'users'), (snapshot) => {
      const usersData = snapshot.docs.map((doc) => ({
        id: doc.id,
        ...doc.data()
      }));
      setUsers(usersData);
    });

    const unsubscribeAuth = auth.onAuthStateChanged(async (user) => {
      try {
        if (user) {
          const userDocRef = doc(firestore, 'admins', user.uid);
          const userDocSnap = await getDoc(userDocRef);
          if (userDocSnap.exists()) {
            const userData = userDocSnap.data();
            setCurrentUser({ ...userData, id: user.uid });
          } else {
            console.error('Current user data not found');
          }
        } else {
          // Reset currentUser state when there is no authenticated user
          setCurrentUser(null);
          console.error('User not authenticated');
        }
      } catch (error) {
        console.error('Error fetching current user:', error);
      }
    });

    return () => {
      unsubscribeUsers();
      unsubscribeAuth();
    };
  }, []);

  useEffect(() => {
    // Listen for changes in conversations and update last message
    const unsubscribe = () => {
      const unsubscribeListeners = users.map((user) => {
        const conversationId = generateConversationId(currentUser?.id, user.id);
        const conversationRef = doc(firestore, 'conversations', conversationId);
        return onSnapshot(conversationRef, (doc) => {
          if (doc.exists()) {
            const lastMessage = doc.data().lastMessage;
            setLastMessages((prev) => ({
              ...prev,
              [user.id]: lastMessage
            }));
          }
        });
      });
      return () => {
        unsubscribeListeners.forEach((unsubscribeListener) => {
          unsubscribeListener();
        });
      };
    };

    return unsubscribe();
  }, [users, currentUser]);

  const handleConversationSelect = (conversation) => {
    const conversationId = generateConversationId(currentUser?.id, conversation.id);
    setSelectedConversation({ ...conversation, conversationId });

    // Reset search term when conversation is selected
    setSearchTerm('');
  };

  const generateConversationId = (userId1, userId2) => {
    const sortedIds = [userId1, userId2].sort();
    return sortedIds.join('_');
  };

  // Function to sort conversations based on the presence of last message and its timestamp
  const sortConversations = (a, b) => {
  const hasLastMessageA = !!lastMessages[a.id] && lastMessages[a.id].timestamp;
  const hasLastMessageB = !!lastMessages[b.id] && lastMessages[b.id].timestamp;

  if (hasLastMessageA && hasLastMessageB) {
    const timestampA = lastMessages[a.id].timestamp.toDate();
    const timestampB = lastMessages[b.id].timestamp.toDate();
    return timestampB - timestampA;
  } else if (hasLastMessageA && !hasLastMessageB) {
    return -1; // Conversation A has last message, so it should come first
  } else if (!hasLastMessageA && hasLastMessageB) {
    return 1; // Conversation B has last message, so it should come first
  } else {
    return 0; // Both conversations have no last message, maintain original order
  }
};
  // Filter users based on search term
  const filteredUsers = users.filter((user) =>
    user.displayName.toLowerCase().includes(searchTerm.toLowerCase())
  );

  // Sort filtered users based on last message
  filteredUsers.sort(sortConversations);

return (
  <div className="chat-container">
    {selectedConversation ? (
      <div className="chat-interface">
        <ChatInterface
          conversation={selectedConversation}
          currentUser={currentUser}
          onBackButtonClick={() => setSelectedConversation(null)}
          otherUser={users.find((user) => user.id === selectedConversation.id)}
        />
      </div>
    ) : (
      <div className="conversation-list-container">
        <div className="conversation-list-text">
          <h2>Conversation List</h2>
          <input
            type="text"
            placeholder="Search for a Student"
            value={searchTerm}
            onChange={(e) => setSearchTerm(e.target.value)}
            className="search-input"
          />
        </div>
        <div className="conversation-list">
          {filteredUsers.map((user) => (
            <div key={user.id} onClick={() => handleConversationSelect(user)} className="user-container">
              <img src={user.photoURL} alt={user.displayName} className="user-photo" />
              <div className="conversation-details">
                <div>
                  <p>{user.displayName}</p>
                  <p>{user.specialty || 'Student'}</p>
                </div>
                {lastMessages[user.id] && (
  <div className="last-message-container">
    <div className={`last-message ${lastMessages[user.id]?.senderId === currentUser.id ? 'you-message' : 'other-message'}`}>
      {lastMessages[user.id]?.senderId === currentUser.id ? 'You: ' : `${user.displayName}: `}
      {lastMessages[user.id]?.text.length > 15 ? lastMessages[user.id]?.text.substring(0, 15) + '...' : lastMessages[user.id]?.text}
    </div>
    <p className={`message-time ${lastMessages[user.id]?.senderId === currentUser.id ? 'you-message-time' : 'other-message-time'}`}>
      {format(lastMessages[user.id]?.timestamp.toDate(), 'h:mm aa')}
    </p>
    {lastMessages[user.id]?.senderId !== currentUser.id && ( // Display notification icon if the other user sent the last message
      <div className="notification-icon">
        <FaBell />
      </div>
    )}
  </div>
)}
              </div>
            </div>
          ))}
        </div>
      </div>
    )}
  </div>
);
};

export default Chat;
