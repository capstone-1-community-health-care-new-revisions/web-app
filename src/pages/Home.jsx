import React from 'react'
import { Link } from 'react-router-dom'

import "../assets/css/home.css"

export default function Home() {
  return (
    <div className="home">
      <div className="image">
        <img src="/svg/medicine-animate.svg" alt="" />
      </div>
      <div className="content">
        <h3>
          Welcome to CommuniCare! <br /> Your Health, Our Passion.
        </h3>
        <p>
          Whether you're seeking preventive care, managing a chronic condition, or
          simply looking to improve your overall health, CommuniCare is your trusted
          partner on your health and wellness journey.
        </p>
        <Link to="/user/student data" className="btn">
          Explore
          <span />
        </Link>
      </div>
    </div>

  )
}
