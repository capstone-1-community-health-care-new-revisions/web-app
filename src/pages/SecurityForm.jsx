import React, { useState } from 'react';
import '../assets/css/SecurityForm.css';

const SecurityForm = ({ onDetailsSubmit }) => {
  const [name, setName] = useState('');
  const [studentNumber, setStudentNumber] = useState('');
  const [age, setAge] = useState('');
  const [gender, setGender] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();
    onDetailsSubmit({ name, studentNumber, age, gender });
  };

  return (
    <div className="security-form-container">
      <div className="security-form">
        <h2>Security Form</h2>
        <form onSubmit={handleSubmit}>
          <label>
            Name:
            <input type="text" value={name} onChange={(e) => setName(e.target.value)} />
          </label>
          <label>
            Student Number:
            <input type="text" value={studentNumber} onChange={(e) => setStudentNumber(e.target.value)} />
          </label>
          <label>
            Age:
            <input type="text" value={age} onChange={(e) => setAge(e.target.value)} />
          </label>
          <label>
            Gender:
            <select value={gender} onChange={(e) => setGender(e.target.value)}>
              <option value="">Select Gender</option>
              <option value="male">Male</option>
              <option value="female">Female</option>
            </select>
          </label>
          <div className="button">
          <button type="submit">Submit</button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default SecurityForm;