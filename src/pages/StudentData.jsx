import React, { useState, useEffect } from 'react';
import '../assets/css/StudentData.css';
import moment from 'moment-timezone';

const StudentData = () => {
  const [userData, setUserData] = useState([]);
  const [filter, setFilter] = useState('');

  const fetchData = async () => {
    try {
      const response = await fetch('http://localhost:2000/api/data');

      if (!response.ok) {
        throw new Error('Failed to fetch user data');
      }

      const data = await response.json();
      setUserData(data);
    } catch (error) {
      console.error('Error fetching user data:', error.message);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleDelete = async (id) => {
    const confirmDelete = window.confirm("Are you sure you want to delete this data?");
    if (!confirmDelete) return;

    try {
      const response = await fetch(`http://localhost:2000/api/data/${id}`, {
        method: 'DELETE',
      });

      if (!response.ok) {
        throw new Error('Failed to delete user data');
      }

      fetchData();
    } catch (error) {
      console.error('Error deleting user data:', error.message);
    }
  };

  const filteredData = userData.filter(user => (
    user.course.toLowerCase().includes(filter.toLowerCase()) ||
    user.name.toLowerCase().includes(filter.toLowerCase())
  ));

  return (
    <div className="survey-container">
      <div className="filter-search-container">
        <h2 className="user-data-title">Student Data</h2>
        <input
          type="text"
          placeholder="Filter by Course or Name"
          value={filter}
          onChange={(e) => setFilter(e.target.value)}
          className="filter-input"
        />
      </div>
      <div className="user-table-container">
        <table className="user-table">
          <thead>
            <tr>
              <th className="column-black">Name</th>
              <th className="column-black">Student Number</th>
              <th className="column-black">Course</th>
              <th className="column-black">Symptoms</th>
              <th className="column-black">Potential Issues</th>
              <th className="column-black">Height</th>
              <th className="column-black">Weight</th>
              <th className="column-black">Age</th>
              <th className="column-black">Submission Date and Time</th>
              <th className="column-black">Actions</th>
            </tr>
          </thead>
          <tbody>
            {filteredData.map((user) => (
              <tr key={user.id}>
                <td className="column-black">{user.name}</td>
                <td className="column-black">{user.student_number}</td>
                <td className="column-black">{user.course}</td>
                <td className="column-black">
                  {user.symptoms.split(',').map((symptom, index) => (
                    <div key={index} className="bubble">{symptom.replace(/["[\]]/g, '')}</div>
                  ))}
                </td>
                <td className="column-black">
                  {user.potential_issues.split(',').map((issue, index) => (
                    <div key={index} className="bubble">{issue.replace(/["[\]]/g, '')}</div>
                  ))}
                </td>
                <td className="column-black">{user.height}</td>
                <td className="column-black">{user.weight}</td>
                <td className="column-black">{user.age}</td>
                <td className="column-black">{moment(user.submission_date_time).tz('Asia/Manila').format('YYYY-MM-DD hh:mm A')}</td>
                <td className="column-black">
                  <button className="delete-button" onClick={() => handleDelete(user.id)}>
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default StudentData;