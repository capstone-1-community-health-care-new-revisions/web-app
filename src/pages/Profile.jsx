import React, { useState, useEffect } from 'react';
import { auth, firestore, storage } from '../firebase';
import '../assets/css/profile.css';

const Profile = () => {
  const [displayName, setDisplayName] = useState('');
  const [idNumber, setIdNumber] = useState('');
  const [specialty, setSpecialty] = useState('');
  const [isEditing, setIsEditing] = useState(false);
  const [profilePhoto, setProfilePhoto] = useState(null);
  
  useEffect(() => {
    const user = auth.currentUser;
    if (user) {
      const userDocRef = firestore.collection('admins').doc(user.uid);
      userDocRef.get().then((doc) => {
        if (doc.exists) {
          const profileData = doc.data();
          setDisplayName(profileData.displayName || '');
          setIdNumber(profileData.idNumber || '');
          setSpecialty(profileData.specialty || '');
          setProfilePhoto(profileData.photoURL || '');
        }
      });
    }
  }, []);

  const handleUpdateProfile = async () => {
    try {
      const user = auth.currentUser;
      if (user) {
        const userDocRef = firestore.collection('admins').doc(user.uid);
        const updatedProfileInfo = {
          displayName: displayName || null,
          idNumber: idNumber || null,
          specialty: specialty || null,
          photoURL: profilePhoto || null,
        };
        await userDocRef.set(updatedProfileInfo, { merge: true });
        console.log('Profile updated successfully!');
        setIsEditing(false);
      } else {
        console.log('User not logged in.');
      }
    } catch (error) {
      console.error('Error updating profile:', error);
    }
  };

  const handleProfilePhotoChange = async (event) => {
    const file = event.target.files[0];
    const maxSizeInBytes = 5 * 1024 * 1024;
    if (file.size > maxSizeInBytes) {
      console.error('File size exceeds the limit (5MB). Please upload a smaller image.');
      return;
    }

    try {
      const storageRef = storage.ref();
      const fileRef = storageRef.child(`${auth.currentUser.uid}/profilePhoto`);
      await fileRef.put(file);
      const downloadURL = await fileRef.getDownloadURL();
      setProfilePhoto(downloadURL);
    } catch (error) {
      console.error('Error uploading profile photo:', error);
    }
  };

  return (
    <div className="profile-page" style={{ backgroundImage: 'url("/images/FAQs bg.png")' }}>
      <div className="profile-container">
        <div className="profile-avatar">
          <label htmlFor="avatar-upload" className="avatar-label">
            <img
              src={profilePhoto || 'https://via.placeholder.com/150'}
              alt="Profile Avatar"
              className="avatar-image"
            />
            <input
              type="file"
              accept="image/*"
              id="avatar-upload"
              onChange={handleProfilePhotoChange}
              disabled={!isEditing}
            />
          </label>
        </div>
        <h2>Profile</h2>
        <div>
          <label htmlFor="displayName">Full Name:</label>
          <input
            type="text"
            id="displayName"
            value={displayName}
            onChange={(e) => setDisplayName(e.target.value)}
            disabled={!isEditing}
          />
        </div>
        <div>
          <label htmlFor="idNumber">ID Number:</label>
          <input
            type="text"
            id="idNumber"
            value={idNumber}
            onChange={(e) => setIdNumber(e.target.value)}
            disabled={!isEditing}
          />
        </div>
        <div className="specialty-container">
          <label htmlFor="specialty">Specialty:</label>
          <select
            id="specialty"
            value={specialty}
            onChange={(e) => setSpecialty(e.target.value)}
            disabled={!isEditing}
          >
            <option value="">Select Specialty</option>
            <option value="School Nurse">School Nurse</option>
            <option value="School Physician">School Physician</option>
          </select>
        </div>

        <div className="button-container">
          {isEditing ? (
            <button className="update-button" onClick={handleUpdateProfile}>
              Update Profile
            </button>
          ) : (
            <button className="edit-button" onClick={() => setIsEditing(true)}>
              Edit Profile
            </button>
          )}
        </div>
      </div>
    </div>
  );
};

export default Profile;
