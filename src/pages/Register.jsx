import React, { useRef, useState } from "react";
import { useAuth } from "../context/AuthContext";
import { Alert } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";


import "../assets/css/register.css";

export default function Register() {
  const emailRef = useRef();
  const passwordRef = useRef();
  const passwordConfirmRef = useRef();
  const { signup } = useAuth();
  const [error, setError] = useState("");
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();

  async function handleSubmit(e) {
    e.preventDefault();

    if (passwordRef.current.value !== passwordConfirmRef.current.value) {
      return setError("Passwords do not match.");
    }

    try {
      setError("Account successfully created.");
      setLoading(true);
      await signup(emailRef.current.value, passwordRef.current.value);
      navigate("/");
    } catch {
      setError("Unable to create an account.");
    }
    setLoading(false);
  }

  return (
    <div className="register-body">
      <div className="register-container">
        <div className="forms">
          <div className="register-form">
          <img src="/images/Untitled design.png" alt="Logo" />
            <span className="title">Registration</span>
            <form onSubmit={handleSubmit}>
              <div className="input-field">
                <input
                  type="email"
                  placeholder="Email"
                  required
                  ref={emailRef}
                />
                <img src="/svg/email.svg" alt="Email" />
              </div>
              <div className="input-field">
                <input
                  type="password"
                  className="password"
                  placeholder="Password"
                  required
                  ref={passwordRef}
                />
                <img src="/svg/lock.svg" alt="Password" />
              </div>
              <div className="input-field">
                <input
                  type="password"
                  className="password"
                  placeholder="Confirm Password"
                  required
                  ref={passwordConfirmRef}
                />
                <img src="/svg/lock.svg" alt="Confirm Password" />
              </div>

              <div className="alert">
                {error && <Alert variant="danger">{error}</Alert>}
              </div>

              <div className="button">
                <button disabled={loading}>Signup</button>
              </div>
            </form>
            <div className="login-signup">
              <span className="text">
                Already a member?
                <Link to="/" className="text login-link">
                  Login
                </Link>
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
