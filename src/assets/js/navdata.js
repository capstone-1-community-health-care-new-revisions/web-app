export const NavLinkData = [
  {
    id: 1,
    label: 'Home'
  },
  {
    id: 2,
    label: 'Student Data'
  },
  {
    id: 3,
    label: 'Chat'
  },
  {
    id: 4,
    label: 'Profile'
  },
  {
    id: 5,
    label: 'FAQs'
  },
]