// faqData.js
const faqsdata = [
  {
    question: 'What is CommuniCare?',
    answer: 'The “CommuniCare” application is created for students. It is an approachable digital platform designed to enhance student health and well-being. It serves as a center of attention for students, improving access to care and promoting general wellness. Through a strong focus on student health and the promotion of a better learning environment, this application helps students perform better academically and develop overall.',
  },
  {
    question: 'Why is CommuniCare created?',
    answer: 'Many students struggle with the challenging issue of not having enough time to prioritize their health because of the responsibilities of ongoing education. Long study sessions that leave little time for self-care and general well-being are typically the result of academic pressure, which makes it difficult to perform well in class. Second, many students are unable to give their health the priority it deserves due to financial constraints and a lack of accessible health options. Since the cost of their education sometimes leaves little room for additional healthcare costs, some students opt not to undergo essential medical checks or treatments.',
  },
  {
    question: 'What services does CommuniCare offer?',
    answer: 'We have been providing care to students as a locally owned and operated, Health Care Service.',
  },
  {
    question: 'What is the importance of regular check-ups?',
    answer: 'Regular check-ups help detect and prevent potential health issues early, ensuring better treatment outcomes. They also help you maintain overall health.',
  },
  {
    question: 'How often should I schedule a check-up?',
    answer: 'The frequency of check-ups depends on your age, gender, and medical history. Typically, an annual check-up is recommended.',
  },
  {
    question: 'What should I do in case of a medical emergency?',
    answer: 'Dial 911 or visit the nearest emergency room immediately if you believe you have a medical emergency.',
  },
  {
    question: 'What is the difference between urgent care and the emergency room?',
    answer: 'Urgent care is for non-life-threatening issues and provides faster service than the ER. The emergency room is for severe, life-threatening conditions.',
  },
  {
    question: 'What are the benefits of regular check-ups and preventive care?',
    answer: 'Regular check-ups can detect health issues early, making them easier to treat. Preventive care can help you avoid serious health problems.',
  },{
    question: 'How can I improve my mental health?',
    answer: 'Strategies for improving mental health include seeking therapy, practicing stress management, and maintaining a healthy lifestyle.',
  },
  {
    question: 'How can I maintain a healthy diet and lifestyle?',
    answer: 'Eating a balanced diet, exercising regularly, getting enough sleep, and managing stress are key to a healthy lifestyle.',
  },
  {
    question: 'How can I manage my medications effectively?',
    answer: 'Organize your medications, follow your providers instructions, and set up reminders to take them on time.',
  },
  {
    question: 'What is the importance of regular exercise for overall health?',
    answer: 'Regular exercise helps maintain a healthy weight, improves cardiovascular health, and reduces the risk of chronic diseases.',
  },
  {
    question: 'How can I manage stress and anxiety in everyday life?',
    answer: 'Stress management techniques include deep breathing, mindfulness, and seeking support from a therapist.',
  },
  {
    question: 'What is the recommended age for various health screenings?',
    answer: 'The recommended age for screenings varies by the type of screening and your individual risk factors. Consult your healthcare provider for guidance.',
  },
  {
    question: 'What is the role of a primary care physician in my healthcare?',
    answer: 'A primary care physician serves as your main point of contact for healthcare, managing routine check-ups and coordinating specialty care.',
  },
  {
    question: 'What is the importance of sleep for overall health?',
    answer: 'Many healthcare providers offer online portals where you can securely access your medical records and test results. Contact your provider for access details.',
  },
];

export default faqsdata;
