import { Outlet, useLocation, Link, useNavigate } from "react-router-dom"; 
import { useState, useEffect } from "react";
import { useAuth } from "../context/AuthContext";
import "../assets/css/navigationbar.css";

import { NavLinkData } from "../assets/js/navdata";

import CloseButton from "../components/CloseButton";
import Hamburger from "../components/Hamburger";
import NavButton from "../components/NavButton";
import Profile from "../components/Profile";
import Footer from "../components/Footer";

const Sidebar = () => {
  const location = useLocation();
  const [isOpen, setIsOpen] = useState(false);
  const { signout } = useAuth(); 
  const navigate = useNavigate(); 
  const handleSignout = async () => {
    try {
      await signout();
      navigate("/");
    } catch (error) {
      console.error("Error signing out", error);
    }
  };

  const handleToggleMenu = () => {
    setIsOpen((prevOpen) => !prevOpen);
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    setIsOpen(false);
  }, [location]);


  return (
    <>
      <div className={isOpen ? "disable" : ""}></div>
      <CloseButton close={handleToggleMenu} isOpen={isOpen} />
      <nav className={isOpen ? "sidebar open" : "sidebar"}>
        <Profile />
        <div className="links">
          {NavLinkData.map((data) => (
            <NavButton key={data.id} label={data.label} />
          ))}
          <Link to="/" onClick={handleSignout}>
            <img src={`/svg/nav_link/logout.svg`} alt="logout" />
            <span>Logout</span>
          </Link>
        </div>
      </nav>
      <header>
        <div className="banner">
          <Hamburger handleToggleMenu={handleToggleMenu} />
        </div>
      </header>
      <div className="content-container">
        <main>
          <Outlet />
        </main>
      </div>
      <Footer />
    </>
  );
};

export default Sidebar;
