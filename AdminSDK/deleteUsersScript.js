const admin = require('firebase-admin');
const serviceAccount = require('./serviceAccountKey.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://community-health-care-uop-default-rtdb.asia-southeast1.firebasedatabase.app',
});

async function deleteAllUsers() {
  try {
    // Get a list of all users
    const users = await admin.auth().listUsers();
    
    // Delete each user
    const deletePromises = users.users.map((user) => admin.auth().deleteUser(user.uid));

    // Wait for all delete operations to complete
    await Promise.all(deletePromises);

    console.log('All users deleted successfully.');
  } catch (error) {
    console.error('Error deleting users:', error);
  }
}

deleteAllUsers();
